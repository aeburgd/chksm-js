# chksm-js

[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![dependencies Status](https://david-dm.org/aburgd/chksm-js/status.svg?style=flat)](https://david-dm.org/aburgd/chksm-js)
[![devDependencies Status](https://david-dm.org/aburgd/chksm-js/dev-status.svg?style=flat)](https://david-dm.org/aburgd/chksm-js?type=dev)
[![Travis](https://img.shields.io/travis/aburgd/chksm-js.svg?style=flat&label=travis)](https://travis-ci.org/aburgd/chksm-js)
[![Greenkeeper badge](https://badges.greenkeeper.io/aburgd/chksm-js.svg)](https://greenkeeper.io/)

chksm, in node, using [terkelg/prompts](https://github.com/terkelg/prompts) :question: and nodejs crypto :lock:
